import { useState, useEffect } from "react";
import {Link} from "react-router-dom";
import {Card, Button} from "react-bootstrap";



// Deconstruct the "courseProp" from the "props" object to shorten the syntax
export default function CourseCard ({courseProp}){

	//check to see if the data was passed succesfully
	// passed props is an object within an object
	// Accessing first object
	// console.log(props)
	// Accessing second object
	// console.log(props.courseProp);
	// console.log(props.courseProp.name);

	// Deconstruct courseProp properties into their own variable
	const {_id, name, description, price, slots} = courseProp;

	// States
		// States are used to keep track the information related to individual component.

	// Hooks
		// Special react defines methods and function that allows us to do certain task in our components.
		// Use the state hook for this components to be able to store its state.

	// Syntax 
		// const [stateName, setStateName] = useState(initialStateValue);

	// useState () is a hook that creates states.
		// useState() returns an array with 2 items:
			// The first item in the array is the state
			// and the second one is the setter function (to change the initial state)

		


		// Array destructuring for the useState()
		// const [count, setCount] = useState(0);
		// console.log(useState(0));

		// Activity Solution
		// const [seats, setSeats] = useState(10);
		// const [isOpen, setIsOpen] = useState();

		// function enroll(){
		// 	if(seats > 0){
		// 		setCount(count + 1);
		// 		console.log("Enrollees: " + count);

		// 		setSeats(seats - 1);
		// 		console.log("Seats: " + seats);
		// 	}
		// 	else{
		// 		alert("No more seats available.")
		// 	}
		// }

		// const [seats, setSeats] = useState(10);
		// const [isOpen, setIsOpen] = useState(false);

		// function enroll(){
		// 	setCount(count + 1);
		// 	// console.log("Enrollees: " + count);

		// 	setSeats(seats - 1);
		// 	// console.log("Seats: " + seats);

		// }

		// function unEnroll(){
		// 	setCount(count - 1);
		// }

		//useEffect()
		//useEffect allows us to run a task or an effect, the difference is that with useEffect we can manipulate when it will run.
		// Syntax
			// useEffect(function, [dependency]);
		// useEffect(() =>{
		// 	if(seats === 0 ){
		// 		alert("No more seats available");
		// 		setIsOpen(true);
		// 	}

		// }, [seats]);

			// If the useEffect() does not have a dependency array, it will run on the initial render and whenever a state is set by its set function.
			// useEffect(()=>{
			// 	// Runs on every render
			// 	console.log("useEffect Render")
			// })

			// If the useEffect() has a dependency array but empty it will only run on inital render.
			// useEffect(()=>{
			// 	// Run only on the initial render
			// 	console.log("useEffect Render")
			// }, [])

			// If the useEffect() has a dependency array and there is state or data in it, the useEffect() will run whenever that state is updated.
			// useEffect(()=>{
			// 	// Run on a initial render.
			// 	// And everytime the dependecy value will change (seats)
			// 	console.log("useEffect Render")
			// }, [seats])


	return (
		<Card className="p-3 my-3">
		  <Card.Body>
		  	<Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description</Card.Subtitle>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Subtitle>Price</Card.Subtitle>
		    <Card.Text>
		      {price}
		    </Card.Text>
		    <Card.Text>
		      Slots: {slots}
		    </Card.Text>
		{/*We will be able to select a specific course through its url*/}
		    <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>

		    {/*<Button variant="primary mx-1" onClick={enroll}>Enroll</Button>
		    <Button variant="danger mx-1" onClick={unEnroll}>Unenroll</Button>*/}
		  </Card.Body>
		</Card>
	)
}