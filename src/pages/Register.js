import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext"
import { Navigate, useNavigate } from "react-router-dom";

import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";


export default function Register(){

	// State hooks to store the value of the input fields

	const{user} = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		e.preventDefault();

		// Checking if the email is still available
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration"
				})
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data){
						// clear inputt fields
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration Succssful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						//redirect the user to the login page after registration.
						navigate("/login");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}

		})

		

		// alert("Thank you for registering!")
		// email = ("");
		// password1 = ("");
		// password2 = ("");
	}

	// State to determine whether  submit button is eneabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:
		// No empty input fields.
		// password & verify password should be the same.

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 &&email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	return(

		(user.id !== null)
		?
			<Navigate to="/courses" />
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit = {(e) => registerUser(e)}>

				<Form.Group className="mb-3" controlId="firstEmail">
				  <Form.Label>First Name</Form.Label>
				  <Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastEmail">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="mobileNo">
				  <Form.Label>Mobile Number</Form.Label>
				  <Form.Control type="number" placeholder="Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
				</Form.Group>

			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)} />
			  </Form.Group>
			  {/*<Button variant="primary" type="submit" disable ={isActive}>
			    Submit
			  </Button>*/}
			  {
			  	isActive
			  	?
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		Submit
			  		</Button>
			  	:
			  		<Button variant="primary" type="submit" id="submitBtn" disabled>
			  		Submit
			  		</Button>
			  }

			</Form>
		</>
	)
}